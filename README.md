# asdf

Install asdf and plugins

## Dependencies

* [polkhan.bash](https://gitlab.com/polkhan/bash.git)
  _The bash configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `version`
    * String
    * Usages: Version of asdf to install

* `plugins`
    * Type: List
    * Usages: List of plugins to install

```
asdf:
  version: 0.6.0
  plugins:
    - helm
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.asdf

## License

MIT
